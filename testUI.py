from ttk import *
import Tkinter as tk
from Tkinter import *
from PIL import Image, ImageTk
import cv2,sys,os
from imutils.video import FPS
from PIL import Image
import imutils
import subprocess
from subprocess import Popen,PIPE,STDOUT,call
import openface,dlib
import math
from tkFileDialog import askopenfilename
import re
import subprocess
from subprocess import Popen,PIPE,STDOUT,call
import time
start = time.time()
import argparse
import pickle
from operator import itemgetter
import numpy as np
np.set_printoptions(precision=2)
import pandas as pd
from sklearn.pipeline import Pipeline
from sklearn.lda import LDA
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import SVC
from sklearn.grid_search import GridSearchCV
from sklearn.mixture import GMM
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from imutils.video import WebcamVideoStream
from imutils.video import FPS
from imutils.video import FileVideoStream

fileDir = os.path.dirname(os.path.realpath(__file__))
modelDir = os.path.join(fileDir, '..', 'models')
dlibModelDir = os.path.join(modelDir, 'dlib')
openfaceModelDir = os.path.join(modelDir, 'openface')

 
def getRep(imgPath, multiple=False):
	start = time.time()
	bgrImg = imgPath
	if bgrImg is None:
		raise Exception("Unable to load image: {}".format(imgPath))

	rgbImg = cv2.cvtColor(bgrImg, cv2.COLOR_BGR2RGB)

	if False:
		print("  + Original size: {}".format(rgbImg.shape))
	if False:
		print("Loading the image took {} seconds.".format(time.time() - start))

	start = time.time()

	if multiple:
		bbs = align.getAllFaceBoundingBoxes(rgbImg)
	else:
		bb1 = align.getLargestFaceBoundingBox(rgbImg)
        bbs = [bb1]
	if len(bbs) == 0 or (not multiple and bb1 is None):
 		raise Exception("Unable to find a face: {}".format(imgPath))
	if False:
		print("Face detection took {} seconds.".format(time.time() - start))

	reps = []
	for bb in bbs:
		start = time.time()
		alignedFace = align.align(
			96,
			rgbImg,
			bb,
			landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)
		#print(alignedFace)
		if alignedFace is None:
			raise Exception("Unable to align image: {}".format(imgPath))
		if False:
			print("Alignment took {} seconds.".format(time.time() - start))
			print("This bbox is centered at {}, {}".format(bb.center().x, bb.center().y))

		start = time.time()
		rep = net.forward(alignedFace)
		if False:
			print("Neural network forward pass took {} seconds.".format(
				time.time() - start))
		reps.append((bb.center().x, rep))
	sreps = sorted(reps, key=lambda x: x[0])

	#print(sreps)
	return sreps




def infer(args, multiple=False):
	global co
	with open(classifier, 'r') as f:
		(le, clf) = pickle.load(f)

	reps = getRep(args, multiple)
	#print(reps)
	if len(reps) > 1:
		print("List of faces in image from left to right")
	for r in reps:
		rep = r[1].reshape(1, -1)
		bbx = r[0]
		start = time.time()
		predictions = clf.predict_proba(rep).ravel()
		maxI = np.argmax(predictions)
		person = le.inverse_transform(maxI)
		confidence = predictions[maxI]
		if False:
			print("Prediction took {} seconds.".format(time.time() - start))
		if multiple:
			print("Predict {} @ x={} with {:.2f} confidence.".format(person, bbx,
                                                                         confidence))
		else:
			#print("{} ==> {:.2f}".format(person, confidence))
			co=co+1
			T.delete("1.0", END)
			T.insert("1.0",person+str(co)+"\n")
			T.grid()
			print(person)
		if isinstance(clf, GMM):
			dist = np.linalg.norm(rep - clf.means_[maxI])
			print("  + Distance from the mean: {}".format(dist))



def show_vid():
	global imageNumber
	if not cap.isOpened():                             
		print("cant open the camera")
	flag, frame = cap.read()
	frame = cv2.flip(frame, 1)
	if flag is None:
		print("Major error!")
	elif flag:
		global last_frame
		last_frame = frame.copy()
   
	detections = detector(frame, 1) #Detect the faces in the image	
	for k,d in enumerate(detections):
		alignedFace = face_aligner.align(400, frame, d, landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)
		#print(alignedFace)
		pic = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)  
		img = Image.fromarray(pic)
		imgtk = ImageTk.PhotoImage(image=img)
		lmain.imgtk = imgtk
		lmain.configure(image=imgtk)
		infer(frame)
	lmain.after(1, show_vid)
 
def captureFromPath():
	fvs = FileVideoStream(fileName).start()
	time.sleep(1.0)
	fps = FPS().start()
	while fvs.more():
		frame = fvs.read()
		frame = imutils.resize(frame, width=450)
		frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		frame = np.dstack([frame, frame, frame])	
		cv2.imshow("frame",frame)
		infer(frame)
		cv2.waitKey(1)
		fps.update()

	fps.stop()
	print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
	print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))
	cv2.destroyAllWindows()
	fvs.stop()


def liveCapture():
	print("[INFO] sampling THREADED frames from webcam...")
	vs = WebcamVideoStream(src=0).start()
	fps = FPS().start()

	while True:
		frame = vs.read()
		frame = imutils.resize(frame, width=400)
		cv2.imshow("Frame", frame)
		infer(frame)
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break
		fps.update()

	fps.stop()
	print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
	print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))
	cv2.destroyAllWindows()
	vs.stop()


selectType = 0
def callbackRadio():
	global selectType
	selectType = v.get()

def callback():
	print(selectType)
	setCameraOption(selectType)

def setCameraOption(selectType):
	if selectType == 1:
		captureFromPath()
	else:
		liveCapture()

def getfile():
	if selectType == 1:
		fileName = askopenfilename() 
		print(fileName)


if __name__ == '__main__': 
	fileName = ""
	faceLandmark = 'models/dlib/shape_predictor_68_face_landmarks.dat'
	nnmodel = 'models/openface/nn4.small2.v1.t7'
	classifier = './generated-embeddings/classifier.pkl'
	align = openface.AlignDlib(faceLandmark)
	net = openface.TorchNeuralNet(nnmodel, imgDim=96,
                                  cuda=False)
	co = 0
	detector = dlib.get_frontal_face_detector() #Face detector
	predictor = dlib.shape_predictor('classifiers/shape_predictor_68_face_landmarks.dat') #Landmark identifier
	face_aligner = openface.AlignDlib('classifiers/shape_predictor_68_face_landmarks.dat')
	fps = FPS().start()

	root=tk.Tk()                                   
	lmain = tk.Label(master=root)
	lmain.grid(column=0, rowspan=20)
	root.title("Driver Drowsiness Detection!")           

	v = IntVar()

	Radiobutton(root, text="Load Video", variable=v, value=1,command=callbackRadio).grid(column=0, row=20)
	Radiobutton(root, text="Live Capture", variable=v, value=2,command=callbackRadio).grid(column=1, row=20)

	b = Button(root, text="START", command=callback).grid(column=3, row=20)
	
	S = Scrollbar(root)
	T = Text(root, height=2, width=30)

	S.config(command=T.yview)
	T.config(yscrollcommand=S.set)
	
	button = Button(root, text="Choose File", fg="black", command=getfile) .grid(row=20,column=4)
	root.mainloop()                             
	cap.release()

