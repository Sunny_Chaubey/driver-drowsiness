import cv2,sys,os
from imutils.video import FPS
from PIL import Image


def getNewImgName(path):
	return path.split("/")[1]

vidcap = cv2.VideoCapture(0)
fps = FPS().start()

imageNumber = 0
while vidcap.isOpened():
	success, image = vidcap.read()
	im = Image.fromarray(image)
	newImagePath = os.path.join('images/', 'imgNew_%d.jpg') % imageNumber
	
	newImgName = getNewImgName(newImagePath)  #imageName
	
	cv2.imwrite(newImagePath, image) 
	fps.update()

	if success:
		cv2.imshow('window-name',image)
	if cv2.waitKey(200) & 0xFF == ord('q'):
		break
	imageNumber += 1

fps.stop()
print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))
vidcap.release()
cv2.destroyAllWindows()
 