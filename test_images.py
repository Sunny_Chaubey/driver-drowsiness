'''
	Created on 10/3/2017.
	Objective: Get All video paths
	Created by : SKR 
'''
import os
import re
import cv2,time
import subprocess
from subprocess import Popen,PIPE,STDOUT,call
import time
start = time.time()
import argparse
import cv2
import os
import pickle
from operator import itemgetter
import numpy as np
np.set_printoptions(precision=2)
import pandas as pd
import openface
from sklearn.pipeline import Pipeline
from sklearn.lda import LDA
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import SVC
from sklearn.grid_search import GridSearchCV
from sklearn.mixture import GMM
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB

fileDir = os.path.dirname(os.path.realpath(__file__))
modelDir = os.path.join(fileDir, '..', 'models')
dlibModelDir = os.path.join(modelDir, 'dlib')
openfaceModelDir = os.path.join(modelDir, 'openface')

video_path = []
def imagePaths():
	for root, dirs, files in os.walk("send"):
		for file in files:
			if file.endswith(".jpg"):
				video_path.append(os.path.join(root, file))
	return video_path

def getRep(imgPath, multiple=False):
    start = time.time()
    bgrImg = imgPath
    if bgrImg is None:
        raise Exception("Unable to load image: {}".format(imgPath))

    rgbImg = cv2.cvtColor(bgrImg, cv2.COLOR_BGR2RGB)

    if False:
        print("  + Original size: {}".format(rgbImg.shape))
    if False:
        print("Loading the image took {} seconds.".format(time.time() - start))

    start = time.time()

    if multiple:
        bbs = align.getAllFaceBoundingBoxes(rgbImg)
    else:
        bb1 = align.getLargestFaceBoundingBox(rgbImg)
        bbs = [bb1]
    if len(bbs) == 0 or (not multiple and bb1 is None):
        raise Exception("Unable to find a face: {}".format(imgPath))
    if False:
        print("Face detection took {} seconds.".format(time.time() - start))

    reps = []
    for bb in bbs:
        start = time.time()
        alignedFace = align.align(
            96,
            rgbImg,
            bb,
            landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)
        if alignedFace is None:
            raise Exception("Unable to align image: {}".format(imgPath))
        if False:
            print("Alignment took {} seconds.".format(time.time() - start))
            print("This bbox is centered at {}, {}".format(bb.center().x, bb.center().y))

        start = time.time()
        rep = net.forward(alignedFace)
        if False:
            print("Neural network forward pass took {} seconds.".format(
                time.time() - start))
        reps.append((bb.center().x, rep))
    sreps = sorted(reps, key=lambda x: x[0])

    #print(sreps)
    return sreps




def infer(args, multiple=False):
    with open(classifier, 'r') as f:
        (le, clf) = pickle.load(f)

    reps = getRep(args, multiple)
    #print(reps)
    if len(reps) > 1:
        print("List of faces in image from left to right")
    for r in reps:
        rep = r[1].reshape(1, -1)
        bbx = r[0]
        start = time.time()
        predictions = clf.predict_proba(rep).ravel()
        maxI = np.argmax(predictions)
        person = le.inverse_transform(maxI)
        confidence = predictions[maxI]
        if False:
            print("Prediction took {} seconds.".format(time.time() - start))
        if multiple:
            print("Predict {} @ x={} with {:.2f} confidence.".format(person, bbx,
                                                                         confidence))
        else:
            #print("{} ==> {:.2f}".format(person, confidence))
            print(person)
        if isinstance(clf, GMM):
            dist = np.linalg.norm(rep - clf.means_[maxI])
            print("  + Distance from the mean: {}".format(dist))



if __name__ == '__main__':
    faceLandmark = 'models/dlib/shape_predictor_68_face_landmarks.dat'
    nnmodel = 'models/openface/nn4.small2.v1.t7'
    classifier = './generated-embeddings/classifier.pkl'
    align = openface.AlignDlib(faceLandmark)
    net = openface.TorchNeuralNet(nnmodel, imgDim=96,
                                  cuda=False)
    paths = imagePaths()
    for im in paths:
    	print(im)
    	ndarray = cv2.imread(im)
        infer(ndarray)


