from ttk import *
import Tkinter as tk
from Tkinter import *
import cv2
from PIL import Image, ImageTk
import os
import numpy as np
import cv2,sys,os
from imutils.video import FPS
from PIL import Image
import subprocess
from subprocess import Popen,PIPE,STDOUT,call
import openface,dlib
import numpy as np
import cv2
import time
import dlib
import math
from tkFileDialog import askopenfilename # Open dialog box

Threshold_eye = 4.5
Threshold_mouth = 20
ynArray =[]
imageNumber = 0
last = ""
fileName = ""
def distanceBetweenMouth(c):
	m_60,m_61,m_62,m_63,m_64,m_65,m_66,m_67 = 0,0,0,0,0,0,0,0
	m_60 = c[59]
	m_61 = c[60]
	m_62 = c[61]
	m_63 = c[62]
	m_64 = c[63]
	m_65 = c[64]
	m_66 = c[65]
	m_67 = c[66]
	x1 = distanceFormula(m_61,m_67)
	x2 = distanceFormula(m_62,m_66)
	x3 = distanceFormula(m_63,m_65)	
	return ((x1+x2+x3)/3)

def mouthPoints():
	return [60,61,62,63,64,65,66,67]


def distanceRightEye(c):
	eR_36,eR_37,eR_38,eR_39,eR_40,eR_41 = 0,0,0,0,0,0
	eR_36 = c[35]
	eR_37 = c[36]
	eR_38 = c[37]
	eR_39 = c[38]
	eR_40 = c[39]
	eR_41 = c[40]
	x1 = distanceFormula(eR_37,eR_41)
	x2 = distanceFormula(eR_38,eR_40)	
	return ((x1+x2)/2)

def distanceLeftEye(c):
	eL_42,eL_43,eL_44,eL_45,eL_46,eL_47 = 0,0,0,0,0,0
	eL_42 = c[41]
	eL_43 = c[42]
	eL_44 = c[43]
	eL_45 = c[44]
	eL_46 = c[45]
	eL_47 = c[46]
	x1 = distanceFormula(eL_43,eL_47)
	x2 = distanceFormula(eL_44,eL_46)	
	return ((x1+x2)/2)



def eyePoints():
	return [36,37,38,39,40,41,42,43,44,45,46,47]

def getClassifier():
	com = './demos/classifier.py infer ./generated-embeddings/classifier.pkl '
	return com
	
def distanceFormula(p0,p1):
	return math.sqrt((p0[0] - p1[0])**2 + (p0[1] - p1[1])**2)
co = 0


def classifyImage(path):
	global last,co,S,T
	createCommand  = str(getClassifier()) + path
	#print(createCommand)
	proc=Popen(createCommand, shell=True, stdout=PIPE, )
	output=proc.communicate()
	#print type(output)
	outputList = list(output) #convert to list  
	predictOp = outputList[0].split("\n")
	print(predictOp[1])
	T.delete("1.0", END)
	T.insert("1.0",predictOp[1]+str(co)+"\n")
	T.grid()
	co=co+1
	if  predictOp[1] == 'yawn':
		ynArray.append(1)
	elif predictOp[1] == 'yawnnot':
		ynArray.append(0)

def getNewImgName(path):
	return path.split("/")[1]

detector = dlib.get_frontal_face_detector() #Face detector
predictor = dlib.shape_predictor('classifiers/shape_predictor_68_face_landmarks.dat') #Landmark identifier
face_aligner = openface.AlignDlib('classifiers/shape_predictor_68_face_landmarks.dat')

fps = FPS().start()

imageNumber = 0

global last_frame                  
last_frame = np.zeros((480, 640, 3), dtype=np.uint8)
global cap



def show_vid():
	global imageNumber
	if not cap.isOpened():                             
		print("cant open the camera")
	flag, frame = cap.read()
	frame = cv2.flip(frame, 1)
	if flag is None:
		print "Major error!"
	elif flag:
		global last_frame
		last_frame = frame.copy()
   
	detections = detector(frame, 1) #Detect the faces in the image	
	for k,d in enumerate(detections):
		alignedFace = face_aligner.align(400, frame, d, landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)
		newImagePath = os.path.join('liveImage/' 'yawnnot_%d.jpg') % imageNumber
		cv2.imwrite(newImagePath, frame)
		imageNumber += 1
		newImgName = getNewImgName(newImagePath) 
		pic = cv2.cvtColor(alignedFace, cv2.COLOR_BGR2RGB)  
		img = Image.fromarray(pic)
		imgtk = ImageTk.PhotoImage(image=img)
		lmain.imgtk = imgtk
		lmain.configure(image=imgtk)
		classifyImage(newImagePath)
	lmain.after(1, show_vid)
 

selectType = 0
def callbackRadio():
	global selectType
	selectType = v.get()

def callback():
	print(selectType)
	setCameraOption(selectType)

def setCameraOption(selectType):
	global cap,fileName
	if selectType == 1:
		cap = cv2.VideoCapture(fileName)
	else:
		cap = cv2.VideoCapture(0)
	show_vid()

def getfile():
	global fileName
	if selectType == 1:
		fileName = askopenfilename() 
		print(fileName)

if __name__ == '__main__': 
	root=tk.Tk()                                   
	lmain = tk.Label(master=root)
	lmain.grid(column=0, rowspan=20)
	root.title("Driver Drowsiness Detection!")           

	v = IntVar()

	Radiobutton(root, text="Load Video", variable=v, value=1,command=callbackRadio).grid(column=0, row=20)
	Radiobutton(root, text="Live Capture", variable=v, value=2,command=callbackRadio).grid(column=1, row=20)

	b = Button(root, text="START", command=callback).grid(column=3, row=20)
	
	S = Scrollbar(root)
	T = Text(root, height=2, width=30)

	S.config(command=T.yview)
	T.config(yscrollcommand=S.set)
	
	button = Button(root, text="Choose File", fg="black", command=getfile) .grid(row=20,column=4)
	root.mainloop()                             
	cap.release()

