import cv2,sys,os
from imutils.video import FPS
from PIL import Image
import subprocess
from subprocess import Popen,PIPE,STDOUT,call
import openface,dlib
import numpy as np
import cv2
import time
import dlib
import math
#To play alert music.
import pygame
#To capture drowsy moment 
import datetime

#Global counter
counter=0
Threshold_eye = 4.5
Threshold_mouth = 20
ynArray =[]



#Decision function
def decide(bool_val):
	
	#Global counter
	global counter
	#if non-drowsy
	if not(bool(bool_val)):
		counter=0
	else:
		counter+=1
		
		#Drowsy counting logic, hardcoded 5 
		if(counter==5):
		
			#Just console print
			print("drowsy!!!")
			timeNow=datetime.datetime.now()
			print(timeNow.strftime("%d-%m-%Y %H:%M:%S"))
			
			#alerting function
			alert()
			
			#For analytics, capture moments
			captureMoment()
			
			counter=0
			


#Alerting function
def alert():
	pygame.init()
	pygame.mixer.init()
	#Keep audio file in the runtime directory
	soundmusic= pygame.mixer.Sound("alert.wav")
	soundmusic.play()
	
	#To produce sound
	time.sleep(5)
	
	
#To capture moments
def captureMoment():
	
	#current Time
	timeNow=datetime.datetime.now()
	strTime=timeNow.strftime("%d-%m-%Y %H:%M:%S")
	
	#Outputing to file
	txtFile = open('drowsy_record.txt', 'a+')
	txtFile.write(strTime)
	txtFile.write("\n")
	txtFile.close()

def distanceBetweenMouth(c):
	m_60,m_61,m_62,m_63,m_64,m_65,m_66,m_67 = 0,0,0,0,0,0,0,0
	m_60 = c[59]
	m_61 = c[60]
	m_62 = c[61]
	m_63 = c[62]
	m_64 = c[63]
	m_65 = c[64]
	m_66 = c[65]
	m_67 = c[66]
	x1 = distanceFormula(m_61,m_67)
	x2 = distanceFormula(m_62,m_66)
	x3 = distanceFormula(m_63,m_65)	
	return ((x1+x2+x3)/3)

def mouthPoints():
	return [60,61,62,63,64,65,66,67]


def distanceRightEye(c):
	eR_36,eR_37,eR_38,eR_39,eR_40,eR_41 = 0,0,0,0,0,0
	eR_36 = c[35]
	eR_37 = c[36]
	eR_38 = c[37]
	eR_39 = c[38]
	eR_40 = c[39]
	eR_41 = c[40]
	x1 = distanceFormula(eR_37,eR_41)
	x2 = distanceFormula(eR_38,eR_40)	
	return ((x1+x2)/2)

def distanceLeftEye(c):
	eL_42,eL_43,eL_44,eL_45,eL_46,eL_47 = 0,0,0,0,0,0
	eL_42 = c[41]
	eL_43 = c[42]
	eL_44 = c[43]
	eL_45 = c[44]
	eL_46 = c[45]
	eL_47 = c[46]
	x1 = distanceFormula(eL_43,eL_47)
	x2 = distanceFormula(eL_44,eL_46)	
	return ((x1+x2)/2)



def eyePoints():
	return [36,37,38,39,40,41,42,43,44,45,46,47]

def getClassifier():
	com = './demos/classifier.py infer ./generated-embeddings/classifier.pkl '
	return com
	
def distanceFormula(p0,p1):
	return math.sqrt((p0[0] - p1[0])**2 + (p0[1] - p1[1])**2)

def classifyImage(path):
	createCommand  = str(getClassifier()) + path
	#print(createCommand)
	proc=Popen(createCommand, shell=True, stdout=PIPE, )
	output=proc.communicate()
	#print type(output)
	outputList = list(output) #convert to list  
	predictOp = outputList[0].split("\n")
	print(predictOp[1])
	if  predictOp[1] == 'yawn':
		ynArray.append(1)
		decide(0)
	elif predictOp[1] == 'yawnnot':
		ynArray.append(0)
		decide(1)


def getNewImgName(path):
	return path.split("/")[1]

vidcap = cv2.VideoCapture('testVideo.avi')
detector = dlib.get_frontal_face_detector() #Face detector
predictor = dlib.shape_predictor('classifiers/shape_predictor_68_face_landmarks.dat') #Landmark identifier
face_aligner = openface.AlignDlib('classifiers/shape_predictor_68_face_landmarks.dat')

fps = FPS().start()

imageNumber = 0

while vidcap.isOpened():
	success, image = vidcap.read()
	print(type(image))
	fps.update()

	if success:
		if image != None:
			gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
			# clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
			# clahe_image = clahe.apply(gray)

			detections = detector(image, 1) #Detect the faces in the image

			for k,d in enumerate(detections): #For each detected face
				shape = predictor(image, d) #Get coordinates
				vec = np.empty([68, 2], dtype = int)
				coor = []
				for i in range(1,68): #There are 68 landmark points on each face
					#cv2.circle(frame, (shape.part(i).x, shape.part(i).y), 1, (0,0,255), thickness=1)
					coor.append([shape.part(i).x, shape.part(i).y])
					vec[i][0] = shape.part(i).x
					vec[i][1] = shape.part(i).y

				#RightEye and LeftEye coordinates
				rightEye = distanceRightEye(coor)
				leftEye = distanceLeftEye(coor)
				eyes = (rightEye + leftEye)/2

				#Mouth coordinates
				mouth = distanceBetweenMouth(coor)

				#print(eyes,mouth)
				alignedFace = face_aligner.align(400, image, d, landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)
				newImagePath = ""
				if eyes <= Threshold_eye or mouth >= Threshold_mouth:
					newImagePath = os.path.join('liveImage/', 'yawn_%d.jpg') % imageNumber 
					print("YAWN")
				else:
					newImagePath = os.path.join('liveImage/' 'yawnnot_%d.jpg') % imageNumber
					print("NOT YAWN")

				cv2.imwrite(newImagePath, alignedFace)
				imageNumber += 1

				cv2.imshow('window-name',alignedFace)
				#im = Image.fromarray(image)
				#newImagePath = os.path.join('liveImage/', 'imgNew_%d.jpg') % imageNumber
			
				newImgName = getNewImgName(newImagePath)  #imageName
				#cv2.imwrite(newImagePath, image) 

				classifyImage(newImagePath)
	
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

fps.stop()
print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))
vidcap.release()
cv2.destroyAllWindows()
print(ynArray)
 
